import warp as wp
import numpy as np

wp.init()

num_points = 1000

@wp.kernel
def length(lengths: wp.array(dtype=float)):
    tid = wp.tid()
    i = float(0)
    j = float(0)
    x = float(0)
    while (i < 10000.0):
        j = float(0.0)
        while (j < 10000.0):
            j += float(1.0)
            x += float(i * j)
        i += float(1.0)
    lengths[tid] = float(tid) * float(x)

lengths = wp.zeros(num_points, dtype=float)

wp.launch(kernel=length,
          dim=num_points,
          inputs=[lengths])

print(lengths); 
