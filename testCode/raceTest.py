import warp as wp
import numpy as np

wp.init()

num_points = 12
device = "cpu"

@wp.kernel
def length(x: wp.array(dtype=float),
           y: wp.array(dtype=float),
           z: wp.array(dtype=float),
           zz: wp.array(dtype=float)):

    # thread index
    tid = wp.tid()
    
    fruits = [0,1,2]
    for x in fruits:
        x = x+1

    # compute distance of each point from origin
    z[tid] = x[tid]+y[tid]
    if tid > 1:
        zz[tid] = zz[tid]+100*zz[tid-1]


# allocate arrays of points
hostX = np.random.rand(num_points, 1)
hostY = np.random.rand(num_points, 1)
hostZ = np.add(hostX, hostY)

x = wp.array(hostX, dtype=float, device=device)
y = wp.array(hostY, dtype=float, device=device)
z = wp.zeros(num_points, dtype=float, device=device)
zz = wp.zeros(num_points, dtype=float, device=device)



# launch kernel
wp.launch(kernel=length,
          dim=len(z),
          inputs=[x, y, z, zz],
          device=device)

print(z)
print(hostZ)
