import sys
sys.path.insert(1, '..')

import warp as wp
import numpy as np

wp.init()

num_points = 12
device = "cpu"

@wp.kernel
def length(x: wp.array(dtype=float),
           y: wp.array(dtype=float),
           z: wp.array(dtype=float)):

    # thread index
    tid = wp.tid()

    # compute distance of each point from origin
    z[tid] = x[tid]+y[tid]


# allocate arrays of points
hostX = np.random.rand(num_points, 1)
hostY = np.random.rand(num_points, 1)
hostZ = np.add(hostX, hostY)

x = wp.array(hostX, dtype=float, device=device)
y = wp.array(hostY, dtype=float, device=device)
z = wp.zeros(num_points, dtype=float, device=device)



# launch kernel
wp.launch(kernel=length,
          dim=len(z),
          inputs=[x, y, z],
          device=device)

print(z)
print(hostZ)
