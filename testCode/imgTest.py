import sys
sys.path.insert(1, '..')

import warp as wp
import numpy as np
import sys
from PIL import Image

wp.init()
device = "cpu"

@wp.kernel
def meanFilter(input: wp.array(dtype=wp.float32, ndim=2),
           output: wp.array(dtype=wp.float32, ndim=2)):

    # indices based on thread index
    i,j = wp.tid()
    
    # pass through for the border
    if i == 0 or i == grid_width-1:
        output[i,j] = input[i,j]
        return
    if j == 0 or j == grid_height-1:
        output[i,j] = input[i,j]
        return

    
    output[i, j] = input[i, j]
    output[i, j] = output[i, j] + input[i-1, j] + input[i+1, j] + input[i, j-1] + input[i, j+1]
    output[i, j] = output[i, j] + input[i-1, j-1] + input[i-1, j+1] + input[i+1, j-1] + input[i+1, j+1]

    output[i, j] = output[i, j] / 9.0

@wp.kernel
def umFilterGray(input: wp.array(dtype=wp.float32, ndim=2),
           output: wp.array(dtype=wp.float32, ndim=2)):
    # indices based on thread index
    i,j = wp.tid()
    
    # pass through for the border
    if i == 0 or i == grid_width-1:
        output[i,j] = input[i,j]
        return
    if j == 0 or j == grid_height-1:
        output[i,j] = input[i,j]
        return
    
    s = input[i, j]
    s = s + input[i-1, j] + input[i+1, j] + input[i, j-1] + input[i, j+1]
    s = s + input[i-1, j-1] + input[i-1, j+1] + input[i+1, j-1] + input[i+1, j+1]
    s = s / 9.0

    g = input[i, j] - s
    output[i, j] = input[i, j] + shScale*g

@wp.kernel
def umFilterColour(input: wp.array(dtype=wp.float32, ndim=3),
           output: wp.array(dtype=wp.float32, ndim=3)):
    # indices based on thread index
    i,j,k = wp.tid()

    # pass through for the alpha channel
    if k == 3:
        output[i,j,k] = input[i,j,k]
        return
    
    # pass through for the border
    if i == 0 or i == grid_width-1:
        output[i,j,k] = input[i,j,k]
        return
    if j == 0 or j == grid_height-1:
        output[i,j,k] = input[i,j,k]
        return
    
    s = input[i,j,k]
    s = s + input[i-1,j,k] + input[i+1,j,k] + input[i,j-1,k] + input[i,j+1,k]
    s = s + input[i-1,j-1,k] + input[i-1,j+1,k] + input[i+1,j-1,k] + input[i+1,j+1,k]
    s = s / 9.0

    g = input[i,j,k] - s
    output[i,j,k] = input[i,j,k] + shScale*g
    output[i,j,k] = wp.clamp(output[i,j,k], 0.0, 255.0)


image = Image.open(sys.argv[1])

# summarize some details about the image
print(image.format)
print(image.size)
print(image.mode)

# show the image
# image.show()

npInData = np.asarray(image, dtype='float32')
print(type(npInData))
print(npInData.dtype)

# summarize shape
imShape = npInData.shape
print(imShape)
print(npInData[283,219])


grid_width = wp.constant(imShape[0])
grid_height = wp.constant(imShape[1])
shScale = wp.constant(0.9)

print("grid_width "+str(imShape[0]))
print("grid_height "+str(imShape[1]))

inWarpImage = wp.from_numpy(npInData, dtype=wp.float32, device=device)
outWarpImage = wp.zeros(shape=npInData.shape, dtype=wp.float32, device=device)

# launch kernel
if (image.mode == 'L'):
    wp.launch(kernel=umFilterGray,
            dim=npInData.shape,
            inputs=[inWarpImage, outWarpImage],
            device=device)
else:
    wp.launch(kernel=umFilterColour,
            dim=npInData.shape,
            inputs=[inWarpImage, outWarpImage],
            device=device)

npOutData = np.asarray(wp.array.numpy(outWarpImage), dtype='float32')
print(type(npOutData))

# # create Pillow image
image2 = Image.fromarray(np.uint8(npOutData))
print(type(image2))

# summarize image details
print(image2.mode)
print(image2.size)
# image2.show()

image2.save('out.png')
