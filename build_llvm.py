import os
import subprocess
import sys

from warp.build_dll import *

# set build output path off this file
base_path = os.path.dirname(os.path.realpath(__file__))
build_path = os.path.join(base_path, "warp")

llvm_project_path = os.path.join(base_path, "external/llvm-project")
llvm_build_path = os.path.join(llvm_project_path, "out/build/")
llvm_install_path = os.path.join(llvm_project_path, "out/install/")


# Fetch prebuilt Clang/LLVM libraries
def fetch_prebuilt_libraries():
    if os.name == "nt":
        packman = "tools\\packman\\packman.cmd"
        packages = {"x86_64": "15.0.7-windows-x86_64-ptx-vs142"}
    else:
        packman = "./tools/packman/packman"
        if sys.platform == "darwin":
            packages = {
                "aarch64": "15.0.7-darwin-aarch64-macos11",
                "x86_64": "15.0.7-darwin-x86_64-macos11",
            }
        else:
            packages = {
                "aarch64": "15.0.7-linux-aarch64-gcc7.5",
                "x86_64": "15.0.7-linux-x86_64-ptx-gcc7.5-cxx11abi0",
            }

    for arch in packages:
        subprocess.check_call(
            [
                packman,
                "install",
                "-l",
                f"./_build/host-deps/llvm-project/release-{arch}",
                "clang+llvm-warp",
                packages[arch],
            ]
        )


def build_from_source_for_arch(args, arch, llvm_source):
    # Check out the LLVM project Git repository, unless it already exists
    if not os.path.exists(llvm_source):
        # Install dependencies
        subprocess.check_call([sys.executable, "-m", "pip", "install", "gitpython"])
        subprocess.check_call([sys.executable, "-m", "pip", "install", "cmake"])
        subprocess.check_call([sys.executable, "-m", "pip", "install", "ninja"])

        from git import Repo

        repo_url = "https://github.com/llvm/llvm-project.git"
        print(f"Cloning LLVM project from {repo_url}...")

        shallow_clone = True  # https://github.blog/2020-12-21-get-up-to-speed-with-partial-clone-and-shallow-clone/
        if shallow_clone:
            repo = Repo.clone_from(repo_url, to_path=llvm_source, single_branch=True, branch="llvmorg-15.0.7", depth=1)
        else:
            repo = Repo.clone_from(repo_url, to_path=llvm_source)
            repo.git.checkout("tags/llvmorg-15.0.7", "-b", "llvm-15.0.7")

    print(f"Using LLVM project source from {llvm_source}")

    # CMake supports Debug, Release, RelWithDebInfo, and MinSizeRel builds
    if args.mode == "release":
        # prefer smaller size over aggressive speed
        cmake_build_type = "MinSizeRel"
    else:
        # When args.mode == "debug" we build a Debug version of warp.dll but
        # we generally don't want warp-clang.dll to be a slow Debug version.
        if args.debug_llvm:
            cmake_build_type = "Debug"
        else:
            # The GDB/LLDB debugger observes the __jit_debug_register_code symbol
            # defined by the LLVM JIT, for which it needs debug info.
            cmake_build_type = "RelWithDebInfo"

    # Location of cmake and ninja installed through pip (see build.bat / build.sh)
    python_bin = "python/Scripts" if sys.platform == "win32" else "python/bin"
    os.environ["PATH"] = os.path.join(base_path, "_build/target-deps/" + python_bin) + os.pathsep + os.environ["PATH"]

    if arch == "aarch64":
        target_backend = "AArch64"
    else:
        target_backend = "X86"

    if sys.platform == "darwin":
        host_triple = f"{arch}-apple-macos11"
        osx_architectures = arch  # build one architecture only
    elif os.name == "nt":
        host_triple = f"{arch}-pc-windows"
        osx_architectures = ""
    else:
        host_triple = f"{arch}-pc-linux"
        osx_architectures = ""

    llvm_path = os.path.join(llvm_source, "llvm")
    build_path = os.path.join(llvm_build_path, f"{args.mode}-{arch}")
    install_path = os.path.join(llvm_install_path, f"{args.mode}-{arch}")

    # Build LLVM and Clang
    cmake_gen = [
        # fmt: off
        "cmake",
        "-S", llvm_path,
        "-B", build_path,
        "-G", "Ninja",
        "-D", f"CMAKE_BUILD_TYPE={cmake_build_type}",
        "-D", "LLVM_USE_CRT_RELEASE=MT",
        "-D", "LLVM_USE_CRT_MINSIZEREL=MT",
        "-D", "LLVM_USE_CRT_DEBUG=MTd",
        "-D", "LLVM_USE_CRT_RELWITHDEBINFO=MTd",
        "-D", f"LLVM_TARGETS_TO_BUILD={target_backend};NVPTX",
        "-D", "LLVM_ENABLE_PROJECTS=clang;lld",
        "-D", "LLVM_ENABLE_RUNTIMES=openmp", 
        "-D", "LLVM_INCLUDE_TOOLS=TRUE", 
        "-D", "LLVM_ENABLE_ZLIB=FALSE", 
        "-D", "LLVM_ENABLE_TERMINFO=FALSE", 
        "-D", "CMAKE_CXX_FLAGS=-D_GLIBCXX_USE_CXX11_ABI=0", 
        "-D", f"CMAKE_INSTALL_PREFIX={install_path}", 
        "-D", f"LLVM_HOST_TRIPLE={host_triple}", 
        "-D", f"CMAKE_OSX_ARCHITECTURES={osx_architectures}",
        # fmt: on
    ]

    subprocess.check_call(cmake_gen, stderr=subprocess.STDOUT)

    cmake_build = ["cmake", "--build", build_path]
    subprocess.check_call(cmake_build, stderr=subprocess.STDOUT)

    cmake_install = ["cmake", "--install", build_path]
    subprocess.check_call(cmake_install, stderr=subprocess.STDOUT)


def build_from_source(args):
    print("Building Clang/LLVM from source...")

    if args.llvm_source_path is not None:
        llvm_source = args.llvm_source_path
    else:
        llvm_source = llvm_project_path

    # build for the machine's architecture
    build_from_source_for_arch(args, machine_architecture(), llvm_source)

    # for Apple systems also cross-compile for building a universal binary
    if sys.platform == "darwin":
        if machine_architecture() == "x86_64":
            build_from_source_for_arch(args, "aarch64", llvm_source)
        else:
            build_from_source_for_arch(args, "x86_64", llvm_source)


# build warp-clang.dll
def build_warp_clang_for_arch(args, lib_name, arch):
    try:
        cpp_sources = [
            "native/clang/clang.cpp",
            "native/crt.cpp",
        ]
        clang_cpp_paths = [os.path.join(build_path, cpp) for cpp in cpp_sources]

        clang_dll_path = os.path.join(build_path, f"bin/{lib_name}")

        if args.build_llvm:
            # obtain Clang and LLVM libraries from the local build
            install_path = os.path.join(llvm_install_path, f"{args.mode}-{arch}")
            libpath = os.path.join(install_path, "lib")
        else:
            # obtain Clang and LLVM libraries from packman
            fetch_prebuilt_libraries()
            libpath = os.path.join(base_path, f"_build/host-deps/llvm-project/release-{arch}/lib")

        for _, _, libs in os.walk(libpath):
            break  # just the top level contains library files

        if os.name == "nt":
            libs.append("Version.lib")
            libs.append(f'/LIBPATH:"{libpath}"')
        else:
            libs = [f"-l{lib[3:-2]}" for lib in libs if os.path.splitext(lib)[1] == ".a"]
            if sys.platform == "darwin":
                libs += libs  # prevents unresolved symbols due to link order
            else:
                libs.insert(0, "-Wl,--start-group")
                libs.append("-Wl,--end-group")
            libs.append(f"-L{libpath}")
            libs.append("-lpthread")
            libs.append("-ldl")

        build_dll_for_arch(
            args,
            dll_path=clang_dll_path,
            cpp_paths=clang_cpp_paths,
            cu_path=None,
            libs=libs,
            arch=arch,
            mode=args.mode if args.build_llvm else "release",
        )

    except Exception as e:
        # output build error
        print(f"Warp Clang/LLVM build error: {e}")

        # report error
        sys.exit(1)


def build_warp_clang(args, lib_name):
    if sys.platform == "darwin":
        # create a universal binary by combining x86-64 and AArch64 builds
        build_warp_clang_for_arch(args, lib_name + "-x86_64", "x86_64")
        build_warp_clang_for_arch(args, lib_name + "-aarch64", "aarch64")

        dylib_path = os.path.join(build_path, f"bin/{lib_name}")
        run_cmd(f"lipo -create -output {dylib_path} {dylib_path}-x86_64 {dylib_path}-aarch64")
        os.remove(f"{dylib_path}-x86_64")
        os.remove(f"{dylib_path}-aarch64")

    else:
        build_warp_clang_for_arch(args, lib_name, machine_architecture())
