import sys
sys.path.insert(1, '..')

import warp as wp
import numpy as np
import os

# print(os.environ)
# import ctypes
# ctypes.CDLL('/home/socs/.local/lib/python3.9/site-packages/warp/bin/warp.so', mode=ctypes.RTLD_GLOBAL)
wp.init()

num_points = 1024
device = "cpu"

@wp.kernel
def length(points: wp.array(dtype=wp.vec3),
           lengths: wp.array(dtype=float)):

    # thread index
    tid = wp.tid()
    
    # compute distance of each point from origin
    lengths[tid] = wp.length(points[tid])


# allocate an array of 3d points
points = wp.array(np.random.rand(num_points, 3), dtype=wp.vec3, device=device)
lengths = wp.zeros(num_points, dtype=float, device=device)

# launch kernel
wp.launch(kernel=length,
          dim=len(points),
          inputs=[points, lengths],
          device=device)

print(lengths)
