import warp as wp
import numpy as np
import sys
from PIL import Image

wp.init()
device = "cpu"

@wp.kernel
def grayFilter(input: wp.array(dtype=wp.float32, ndim=2),
           output: wp.array(dtype=wp.float32, ndim=2)):

    # indices based on thread index
    i,j = wp.tid()
    
    # pass through for the border for simplicity
    if i == 0 or i == grid_width-1:
        output[i,j] = input[i,j]
        return
    if j == 0 or j == grid_height-1:
        output[i,j] = input[i,j]
        return

    #copy the pixel itself into the outoput
    output[i, j] = input[i,j]
    # we make a small contribution ot the output using the 4-neighbourhood
    output[i, j] = output[i, j] + 0.1*input[i-1, j] + 0.1*input[i+1, j] + 0.1*input[i, j-1] + 0.1*input[i, j+1]

    # we don't scale the kernel, so the weights add up to more than 1 and the image will be brightened overall
    # since the image will end up being brighter, we want to make sure we don't exceed the max brightness of 255
    output[i,j] = wp.clamp(output[i,j], 0.0, 255.0)

@wp.kernel
def colourFilter(input: wp.array(dtype=wp.float32, ndim=2),
           output: wp.array(dtype=wp.float32, ndim=2)):

    # indices based on thread index
    i,j,k = wp.tid()
    
    # pass through for the border for simplicity
    if i == 0 or i == grid_width-1:
        output[i,j] = input[i,j]
        return
    if j == 0 or j == grid_height-1:
        output[i,j] = input[i,j]
        return

    #copy the pixel itself into the outoput
    output[i, j, k] = input[i,j, k]
    # we make a small contribution ot the output using the 4-neighbourhood
    output[i, j, k] = output[i, j, k] + 0.1*input[i-1, j, k] + 0.1*input[i+1, j, k] + 0.1*input[i, j-1, k] + 0.1*input[i, j+1, k]

    # we don't scale the kernel, so the weights add up to more than 1 and the image will be brightened overall
    # since the image will end up being brighter, we want to make sure we don't exceed the max brightness of 255
    output[i,j, k] = wp.clamp(output[i,j, k], 0.0, 255.0)

image = Image.open(sys.argv[1])

# summarize some details about the image
print(image.format)
print(image.size)
print(image.mode)

# show the image
# image.show()

npInData = np.asarray(image, dtype='float32')
print(type(npInData))
print(npInData.dtype)

# summarize shape
imShape = npInData.shape
print(imShape)
print(npInData[283,219])


grid_width = wp.constant(imShape[0])
grid_height = wp.constant(imShape[1])
shScale = wp.constant(0.9)

print("grid_width "+str(imShape[0]))
print("grid_height "+str(imShape[1]))

inWarpImage = wp.from_numpy(npInData, dtype=wp.float32, device=device)
outWarpImage = wp.zeros(shape=npInData.shape, dtype=wp.float32, device=device)

# launch kernel
if (image.mode == 'L'):
    wp.launch(kernel=umFilterGray,
            dim=npInData.shape,
            inputs=[inWarpImage, outWarpImage],
            device=device)
else:
    wp.launch(kernel=umFilterColour,
            dim=npInData.shape,
            inputs=[inWarpImage, outWarpImage],
            device=device)

npOutData = np.asarray(wp.array.numpy(outWarpImage), dtype='float32')
print(type(npOutData))

# # create Pillow image
image2 = Image.fromarray(np.uint8(npOutData))
print(type(image2))

# summarize image details
print(image2.mode)
print(image2.size)
# image2.show()

image2.save('out.png')
