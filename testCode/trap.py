import warp as wp
import numpy as np
import sys

wp.init()
device = "cpu"

@wp.func
def fK(x: float):
   return x*x

@wp.kernel
def kernel_trap(trapK: wp.array(dtype=float)):
    tid = wp.tid()
    
    if 0 < tid and tid < nK:
        my_x = aK + float(tid) * hK
        my_trap = fK(my_x)
        wp.atomic_add(trapK, 0, my_trap)

def f(x):
    return x * x

def serial_trap(a,b,n):
    h = (b-a) / n
    trap = 0.5*(f(a) + f(b))

    for i in range(1, n):
        x = a + i*h
        trap += f(x)
    
    trap = trap * h

    return trap


a = float(sys.argv[1])
b = float(sys.argv[2])
n = int(sys.argv[3])

ref = serial_trap(a, b, n)
print(ref)

trap_p = 0.5*(f(b) + f(b))
h = (b-a) / n

aK = wp.constant(a)
bK = wp.constant(b)
nK = wp.constant(float(n))
hK = wp.constant(h)

trap_np=np.zeros(1)
trap_np[0] = trap_p
trapK = wp.array(trap_np, dtype=float, device=device)


wp.launch(kernel=kernel_trap,
          dim=n,
          inputs=[trapK],
          device=device)

trap_np_out = wp.array.numpy(trapK)
trap_p = h * trap_np_out[0]
print(trap_p)



